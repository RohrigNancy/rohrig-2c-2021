/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto3_MedidorDistancia.h"       /* <= own header */

#include "gpio.h"
#include "systemclock.h"
#include "medidor_distancia.h"
#include "led.h"
#include <DisplayITS_E0803.h>
#include "switch.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"





/*==================[macros and definitions]=================================*/
#define ON 1
#define OFF 0

/*==================[internal data definition]===============================*/
bool encendido=false;
bool hold=true;
int16_t distance_cm, distance_in;
gpio_t pins[CANT_PINES]={GPIO_LCD_1,GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5};

/*==================[external data definition]===============================*/

void MedidorDist(void);
/*==================[external data definition]===============================*/
//uint8_t blinking = ON;
timer_config my_timer = {TIMER_A,1000,&MedidorDist};
serial_config UART_USB = {SERIAL_PORT_PC, 115200, NULL};
/*UART_USB.baud_rate=115200;
 * UART_USB.port=SERIAL_PORT_PC;
 * UART_USB.pSerial=RS232Send;
 */
/*==================[internal functions declaration]=========================*/
//void do_blink(void); tengo que hacer yo mi función

void SisInit(void)
{
	SystemClockInit();
	LedsInit();
	//SwitchesInit();
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&UART_USB);
}


/*==================[internal functions declaration]=========================*/
void encendido_tec(void){

	encendido=!encendido;
}


/*==================[internal functions declaration]=========================*/
void hold_tec(void){

	hold=!hold;
}

/*==================[external functions definition]==========================*/

void MedidorDist(void)
{
	SystemClockInit();
  	LedsInit();
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	SwitchesInit();
	ITSE0803Init (pins);

	SwitchActivInt(SWITCH_1, encendido_tec);
	SwitchActivInt(SWITCH_2, hold_tec);

		   if (encendido) {
			//Parte 2 --> mostrar el valor  en cm en el display
			distance_cm = HcSr04ReadDistanceCentimeters(); //pasa los valores a cm
			distance_in = HcSr04ReadDistanceInches();

		       if (hold){
		    	  ITSE0803DisplayValue (distance_cm);
			   // Muestro distancia medida utilizando los leds:
			   		 if (distance_cm>0 && distance_cm<10) {
			   			LedOn(LED_RGB_B);
			            LedOff(LED_1);
			   		    LedOff(LED_2);
			   		    LedOff(LED_3);
			   		 }
			   		   else if (distance_cm>=10 && distance_cm<20){
			   				LedOn(LED_RGB_B);
			   				LedOn(LED_1);
			   				LedOff(LED_2);
			   				LedOff(LED_3);}
			   		     else if (distance_cm>=20 && distance_cm<30){
			   		 			LedOn(LED_RGB_B);
			   					LedOn(LED_1);
			   					LedOn(LED_2);
			   					LedOff(LED_3);}
			   		        else if (distance_cm>=30){
			   					    LedOn(LED_RGB_B);
			   						LedOn(LED_1);
			   						LedOn(LED_2);
			   						LedOn(LED_3); }
		                    }}
		       else {
		    	   LedOff(LED_RGB_B);
		    	   LedOff(LED_1);
		    	   LedOff(LED_2);
		    	   LedOff(LED_3);
		    	   ITSE0803DisplayValue (0);
		       }
}
/*==================[external data definition]===============================*/



/*==================[external functions definition]==========================*/

int main(void){

	SisInit();


	while(1){

	//utilizar TEC1 para activar y detener la medición
		}

	return 0;

}

/*==================[end of file]============================================*/

