/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Albano Peñalva
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Proyecto2_MedidorDistancia.h"       /* <= own header */

#include "gpio.h"
#include "systemclock.h"
#include "medidor_distancia.h"
#include "led.h"
#include <DisplayITS_E0803.h>
#include "switch.h"
#include "bool.h"




/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/


/*==================[external data definition]===============================*/



/*==================[external functions definition]==========================*/

int main(void){
  	SystemClockInit();
  	LedsInit();
	HcSr04Init(GPIO_T_FIL2, GPIO_T_FIL3);
	SwitchesInit();
	gpio_t pins[CANT_PINES]={GPIO_LCD_1,GPIO_LCD_2,GPIO_LCD_3,GPIO_LCD_4,GPIO_1,GPIO_3,GPIO_5};
	ITSE0803Init (pins);
	int8_t tecla;
	int16_t distance_cm, distance_in;
	bool encendido=false;
	bool hold=true;
	while(1){
	//Parte 3 y 4 con banderas =>
	tecla=SwitchesRead();
	   switch (tecla)
	   {
	         case SWITCH_1:
	        	 encendido =! encendido;
	         break;

	         case SWITCH_2:
	         	  hold =! hold;
	         break;
	    }

	   if (encendido) {
		//Parte 2 --> mostrar el valor  en cm en el display
		distance_cm = HcSr04ReadDistanceCentimeters(); //pasa los valores a cm
		distance_in = HcSr04ReadDistanceInches();

	       if (hold){
	    	  ITSE0803DisplayValue (distance_cm);
		   // Muestro distancia medida utilizando los leds:
		   		 if (distance_cm>0 && distance_cm<10) {
		   			LedOn(LED_RGB_B);
		            LedOff(LED_1);
		   		    LedOff(LED_2);
		   		    LedOff(LED_3);
		   		 }
		   		   else if (distance_cm>=10 && distance_cm<20){
		   				LedOn(LED_RGB_B);
		   				LedOn(LED_1);
		   				LedOff(LED_2);
		   				LedOff(LED_3);}
		   		     else if (distance_cm>=20 && distance_cm<30){
		   		 			LedOn(LED_RGB_B);
		   					LedOn(LED_1);
		   					LedOn(LED_2);
		   					LedOff(LED_3);}
		   		        else if (distance_cm>=30){
		   					    LedOn(LED_RGB_B);
		   						LedOn(LED_1);
		   						LedOn(LED_2);
		   						LedOn(LED_3); }
	                    }}
	       else {
	    	   LedOff(LED_RGB_B);
	    	   LedOff(LED_1);
	    	   LedOff(LED_2);
	    	   LedOff(LED_3);
	    	   ITSE0803DisplayValue (0);

	       }

	}



	//utilizar TEC1 para activar y detener la medición



	return 0;

}

/*==================[end of file]============================================*/

