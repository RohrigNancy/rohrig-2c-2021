/**! @mainpage Recuperatorio2C2021_RohrigNancy
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Chanel	 	| 	CH1 		|
 * | 	GND  	 	| 	GND 		|
 * | 	+3.3V 	 	| 	+3.3V 		|
 * | 	 DOUT 	    | GPIO_T_COL0 	|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Rohrig Nancy Analia
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Recuperatorio2C2021_RohrigNancy.h"       /* <= own header */

#include "gpio.h"
#include "systemclock.h"
#include "led.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "Tcrt5000.h"
#include "Balanza.h"


#ifndef NULL
#define NULL ((void *) 0)
#endif
/*==================[macros and definitions]=================================*/
#define ON 1
#define OFF 0
#define COUNT_DELAY 1000
#define BUFFER_SIZE 231
#define peso_max_carga 20 //20kg



/*==================[External functions definition]=========================*/

/** @fn bool SisInit (void)
 * @brief Initialization all the functions necessary to carry out the objectives
 *
 * @return nothing return (void)
 */
void SisInit(void);

/** @fn bool menu_serie_UART (void)
 * @brief I communicates with the user from UART
 * It asks for the current date including the time, it also reveals a menu with possible options
 * @param [in] No parameter
 * @return TRUE if no error
 */
bool menu_serie_UART(void);

/** @fn bool Proceso (void);
  * @brief with flags turns the system on or off
  * guides the process of the entire project with a timer
  * @param [in] No parameter
  * @return TRUE if there is no error
*/
bool Proceso (void);

/*==================[External functions definition]=========================*/
/** @fn bool CintasTransportadoras (void);
  * @brief turns on the leds that simulate the tapes
  * @param [in] No parameter
  * @return TRUE or FALSE depending on the tape that is active
*/
bool CintasTransportadoras (void);

/** @fn bool calculo_max_min (void);
  * @brief calculates the maximum and minimum times it takes to load each box
  * @param [in] No parameter
  * @return TRUE if there is no error
*/
bool calculo_max_min (void);


/*==================[internal data definition]===============================*/
uint16_t Peso_val = 0;
uint16_t tiempo_llenado[15]={0};
uint16_t tiempo_llenado_min=0;
uint16_t tiempo_llenado_max=0;
uint16_t aux_cont=0;
bool encendido=false; //banderas
bool hold=true;
/*==================[external data definition]===============================*/
//para tomar temperatura y humedad
timer_config my_timer ={ TIMER_A, 1000, &Proceso};


serial_config UART_USB = { SERIAL_PORT_PC, 115200,&menu_serie_UART};


/*==================[external functions declaration]=========================*/

void SisInit(void) {
	SystemClockInit();
	LedsInit();

	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartInit(&UART_USB);

	Tcrt5000Init(GPIO_T_COL0);
	BalanzaInit();
}

/*==================[internal functions declaration]=========================*/
void encendido_tec(void){

	encendido=!encendido;
}


void hold_tec(void){
	hold=!hold;
}

bool CintasTransportadoras (void) {
	bool detecto_caja=Tcrt5000State();
	/* si el sensor infrarrojo lo detecta, entonces esta encendida
	   la cinta transportadora 1, si no lo detecta, se enciende la 2
	*/
	if(detecto_caja==true) {
		LedOff(LED_2);
		LedOn(LED_1); }
	else {
		LedOff(LED_1);
		LedOn(LED_2);
	}

  return detecto_caja;
}


bool calculo_max_min (void){
	tiempo_llenado_max=tiempo_llenado[0];
	for (uint8_t i=1; i<15; i++) {
		if (tiempo_llenado_max>tiempo_llenado[i]) {
			tiempo_llenado_max=tiempo_llenado[i];
			}
		else {
			tiempo_llenado_min=tiempo_llenado[i];
		     }
		}
	return true;
}
/*==================[external functions declaration]=========================*/

uint16_t j=0; //ayuda para guardar en el vector
bool Proceso (void) {
	if (encendido) {
		bool detecto_caja=CintasTransportadoras();

		if (hold){
			if(detecto_caja==false) {
					//esta en posicion de llenado (cinta T dos)
					Peso_val=BalanzaRead();
					if (Peso_val!=peso_max_carga) {
						aux_cont=aux_cont+1;

						}
					else {
						//se detiene cinta trans. 2 y se activa la 1:
						LedOff(LED_2);
						LedOn(LED_1);
						tiempo_llenado[j]=aux_cont;
						}
			 }
		  }
	  }
}
/*==================[external functions declaration]=========================*/

bool menu_serie_UART(void) {

	if (hold){
		uint8_t dat;
	    UartReadByte(SERIAL_PORT_PC, &dat);
		uint16_t aux_ncajas=1;
			for(uint16_t i=0; i<15; i++) {
				UartSendString(SERIAL_PORT_PC, "Tiempo de llenado de caja ");
				UartSendString(SERIAL_PORT_PC, UartItoa(aux_ncajas, 10));
				UartSendString(SERIAL_PORT_PC, ": ");
				UartSendString(SERIAL_PORT_PC, UartItoa(tiempo_llenado[i], 10));
				UartSendString(SERIAL_PORT_PC, " seg");
				aux_ncajas=aux_ncajas+1;
			}

		UartSendString(SERIAL_PORT_PC, "Tiempo de llenado máximo: ");
		UartSendString(SERIAL_PORT_PC, UartItoa(tiempo_llenado_max, 10));
		UartSendString(SERIAL_PORT_PC, " seg");
		UartSendString(SERIAL_PORT_PC, "Tiempo de llenado mínimo: ");
		UartSendString(SERIAL_PORT_PC, UartItoa(tiempo_llenado_min, 10));
		UartSendString(SERIAL_PORT_PC, " seg");

	}

	return TRUE;
}


/*==================[external functions definition]==========================*/

int main(void) {

	SisInit();
	calculo_max_min ();


	while (1) {

		//Recuperatorio 11/11/2021
	}

	return 0;

}

/*==================[end of file]============================================*/

