/*! @mainpage Recuperatorio2C2021_RohrigNancy
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/112021  | Document creation		                         |
 * |         	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Rohrig Nancy
 *
 */

#ifndef _Recuperatorio2C2021_RohrigNancy_H
#define _Recuperatorio2C2021_RohrigNancy_H

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/


/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

