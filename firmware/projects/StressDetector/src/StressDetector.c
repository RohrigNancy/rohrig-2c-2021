/**! @mainpage StressDetector
 * Proyecto libre
 *
 * \section genDesc General Description
 *
 * El programa tendrá como finalidad general detectar ciertos parametros que puedan suponer un aumento del estrés.
 * Para ello se medirá la temperatura y humedad ambiente, se le pedirá al usuario que ingrese fechas que considere críticas
 * y se hara un registro de la frecuencia cardiaca. Una vez detectado estos valores, si el ambiente es propenso a estres,
 * comenzara a sonar una canción "relajane" por un parlante conectado a la EDU_CIAA.
 *
 * \section hardConn Hardware Connection
 *
 * | SI7007: sensor temp./humed.|   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	select		 	     	|  GPIO_T_FIL0	|
 * | 	Vcc: 3.3V	 			| 	3.3V		|
 * | 	PWM2	 				| 	CH1			|
 * | 	PWM1		 			| 	NC			|
 * | 	GND		 			    | 	GND 		|
 *
 * | NeoPixel: Batería de leds  |   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	DIN      	 	     	|   GPIO_1    	|
 * | 	5V  		 			| 	5V			|
 * | 	GND		 			    | 	GND 		|
 *
 * |   Conector para parlante   |   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	DAC     	 	     	|   DAC     	|
 * | 	GND		 			    | 	GND 		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                                             |
 * |:----------:|:------------------------------------------------------------------------|
 * | 01/10/2021 | Document creation		                        					 	  |
 * | 15/10/2021	| Se empieza código de driver SI7007    		     			    	  |
 * | 21/10/2021	| Se completa codigo de comunicación puerto serie                         |
 * | 22/10/2021	| Se detectan errores en driver y UART							      	  |
 * | 31/10/21	| Se agrega song.h y código para que suene el parlante					  |
 * | 04/11/21	| Se modifica el cógigo agregando información para su mejor entendimiento |
 * | 12/11/21   | Se agrego batería de leds y detector de FC (no se realizaron pruebas)   |
 *
 * @author Rohrig Nancy
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/StressDetector.h"       /* <= own header */
#include "gpio.h"
#include "systemclock.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "led.h"


#include <SI7007.h>
#include "sapi_rtc.h"
#include "NeoPixel.h"
#include "song.h"
#include "analog_io.h"
#include "highpass.h"
#include "lowpass.h"
#include "iir.h"
#include "ili9341.h"


/*==================[macros and definitions]=================================*/
#define ON 1
#define OFF 0
#define COUNT_DELAY 6000
#define BUFFER_SIZE 231
#define MAX_TEMP 29
#define MAX_HUMID 85
//colores de referencia
#define COLOR_G 1
#define COLOR_B 1
#define COLOR_R 1
#define COLOR_NULL 0
#define BRIGHTNESS 100
//Para valores de frecuencia cardiaca
#define FREC_MAX 110
#define FREC_MIN 50
#define CONTADOR 50
#define ANCHO_VENTANA  256
#define TIEMPO_MUESTREO 4
//#define max(a, b) (((a) > (b)) ? (a) : (b))
#define abs(a) (((a) < (0)) ? (-a) : (a))
#define wL_HP NL_HP
#define wL_LP NL_LP



/*==================[macros and definitions]=================================*/

bool encendido=false;
bool apagado=false;
float temperature = 0;
float humidity = 0;


//struct utilizado para guardar fechas críticas
typedef struct {
	int16_t diaa;
	int16_t mess;
} tiempo_crit;

//para que me pase fecha crítica
typedef enum {
	Dia, Mes
} tiempo_c;
tiempo_c time_val = Dia;

tiempo_crit dates_saved;
bool dates_total = false;
bool dates_total_HT = false;
rtc_t hora_config;

//utilizado para guardar datos de fecha actual de la uart
typedef enum {
	hora,segundos, minutos, dia, mes, anio
} tiempo_t;


bool set_clock = FALSE;
bool set_data_critical = FALSE;
bool set_temp_humid = FALSE;
bool set_frecuencia = FALSE;
//para el switch
tiempo_t time_var = hora;

//tipo struct para valores cardiacos
typedef struct {
	uint8_t Frec_Cardiaca;
	uint8_t Frec_max;
	uint8_t Frec_min;
} datos_cardiacos;
datos_cardiacos *val_cardiaco;

uint16_t ventana_filtrada [ANCHO_VENTANA];
uint16_t i;
int j=0;
int a=0;
uint16_t output; // salida filtro pasa bajo
uint16_t output2; // salida filtro pasa alto
uint16_t dato=0;

double w_HP[wL_HP];   	// vector de estados internos del filtro pasa altos
double w_LP[wL_LP];   	// vector de estados internos del filtro pasa bajos
uint16_t dato_frec =0; // dato pasado por filtro de frecuencia
uint16_t dato_deriv=0;  // dato pasado por filtro derivativo
uint32_t dato_cuadratico =0; // dato pasado por filtro cuadratico
uint16_t dato_anterior = 0; // dato para comparar en filtro derivativo
uint16_t umbral =0 ;

const uint16_t ecg_data[ANCHO_VENTANA]={
76, 83, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
 99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
 74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

uint8_t FC_MIN=120; // Designo un valor alto, para que la primer comparación funcione
uint8_t FC_MAX=40; // Designo un valor bajo, para que la primer comparación funcione
uint8_t Frecuencia_Cardiaca = 0;


//funciones involucradas
void SisInit(void);                  //inicialización
bool menu_serie_UART(void);          //comunicación
bool TakeDate(void);                 //avisa si debe sonar la música
void doTimer(void);                  //suena
bool IsRealTimer(tiempo_crit datt);  //valores anteriores a fecha crítica
bool CalculateHTCritical(void);      //true or false si hay temp. y humed. crítica
bool CalculateCriticalDates(tiempo_crit dates_saved); //true or false si fecha crítica
void encendido_tec(void);            //bandera de encendido música
void Delay(void);                    //espera


/*==================[internal data definition]===============================*/
//para tomar temperatura y humedad
timer_config my_timerTH = { TIMER_B, TIEMPO_MUESTREO, &TakeDate};

//para la música que sonará
timer_config my_timer = { TIMER_C, 125,		// 8kHz
		&doTimer };

//UART tiempo real:
serial_config UART_USB = { SERIAL_PORT_PC, 115200, &menu_serie_UART };

/*==================[external functions declaration]=========================*/

/*==================[internal functions declaration]=========================*/
/*Bandera para encender parlante*/
void encendido_tec(void){

	if((hora_config.month!=0)&&(dates_saved.mess!=0)) {
	     encendido=!encendido; }
}

/*Provoca un retardo en la toma de temperatura/humedad del SI7007 */
void Delay(void) {
	uint32_t i;

	for (i = COUNT_DELAY; i != 0; i--) {
		asm ("nop");
	}
}


/*==================[external functions declaration]=========================*/

/*Comunicación con el usuario */
/** @fn bool menu_serie_UART (void)
 * @brief I communicates with the user from UART
 * It asks for the current date including the time, it also reveals a menu with possible options
 * @param [in] No parameter
 * @return TRUE if no error
 */
bool menu_serie_UART(void) {
	uint8_t dat;
	UartReadByte(SERIAL_PORT_PC, &dat);
	/******* RECUPERO VALORES NUMERICOS **********/
	if (set_clock) {
		switch (time_var) {
		case hora:
			hora_config.hour = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese los segundos:  \r\n");
			time_var = segundos;
			break;
		case segundos:
			hora_config.sec = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese los minutos:  \r\n");
			time_var = minutos;
			break;
		case minutos:
			hora_config.min = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el día:  \r\n");
			time_var = dia;
			break;
		case dia:
			hora_config.mday = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el mes:  \r\n");
			time_var = mes;
			break;
		case mes:
			hora_config.month = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el año:  \r\n");
			time_var = anio;
			break;
		case anio:
			hora_config.year = dat;
			UartSendString(SERIAL_PORT_PC, "Son las:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.hour, 10));
			UartSendString(SERIAL_PORT_PC, ":");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.min, 10));
			UartSendString(SERIAL_PORT_PC, ":");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.sec, 10));
			UartSendString(SERIAL_PORT_PC, "  del:  ");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.mday, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.month, 10));
			UartSendString(SERIAL_PORT_PC, "/");
			UartSendString(SERIAL_PORT_PC, UartItoa(hora_config.year, 10));
			UartSendString(SERIAL_PORT_PC, "\r\n");
			time_var = hora;
			dat = 0;
			set_clock = FALSE;
			LedOff(LED_RGB_B);
			break;
		}

	};

	if (set_data_critical) {
		switch (time_val) {
		case Dia:
			dates_saved.diaa = dat;
			UartSendString(SERIAL_PORT_PC, "Ingrese el mes:  \r\n");
			time_val = Mes;
			break;
		case Mes:
			dates_saved.mess = dat;
			UartSendString(SERIAL_PORT_PC,
					"Los datos ingresaron correctamente \r\n");
			dat = 0;
			set_data_critical = FALSE;
			LedOff(LED_RGB_R);
			break;
		}

	};


	if (set_temp_humid) {
		UartSendString(SERIAL_PORT_PC, "La temperatura ambiente es:  \r");
		UartSendString(SERIAL_PORT_PC, UartItoa(temperature, 10));
		UartSendString(SERIAL_PORT_PC, " \r grados celsuis  \r\n");
		UartSendString(SERIAL_PORT_PC, "La humedad ambiente es:  \r");
		UartSendString(SERIAL_PORT_PC, UartItoa(humidity, 10));
		UartSendString(SERIAL_PORT_PC, "\r % \r\n");
		dat = 0;
		set_temp_humid = FALSE;
		LedOff(LED_RGB_G);

		};
	if (set_frecuencia) {
			UartSendString(SERIAL_PORT_PC, "La frecuencia cardiaca es:  \r");
			UartSendString(SERIAL_PORT_PC, UartItoa(val_cardiaco->Frec_Cardiaca, 10));
			UartSendString(SERIAL_PORT_PC, " \r lpm  \r\n");
			dat = 0;
			set_frecuencia = FALSE;
			LedOff(LED_RGB_R);

			}


	/******* RECUPERO COMANDOS **********/
	switch (dat) {
	case 't': //colocar mí código de uart fecha crítica abajo de swith
		set_temp_humid = TRUE;
		LedOn(LED_RGB_G);
		break;
	case 'd':
		UartSendString(SERIAL_PORT_PC, "Ingrese la hora actual: \r\n");
		set_clock = TRUE;
		LedOn(LED_RGB_B);
		break;
	case 'f': //colocar mí código de uart fecha crítica abajo de swith
		UartSendString(SERIAL_PORT_PC, "Ingrese el dia: \r\n ");
		UartSendString(SERIAL_PORT_PC, "\r\n");
		set_data_critical = TRUE;
		LedOn(LED_RGB_R);
		break;
	case 'm':
		UartSendString(SERIAL_PORT_PC, "/---------------MENU------------/\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'm' para ver el menu:\r\n");
		UartSendString(SERIAL_PORT_PC,
				"Ingrese 't' para que muestre la temperatura y la humedad ambiente: \r\n");
		UartSendString(SERIAL_PORT_PC,
				"Ingrese 'd' para configurar la hora:\r\n");
		UartSendString(SERIAL_PORT_PC, "Ingrese 'c' para que muestre la frecuencia cardiaca:\r\n");
		UartSendString(SERIAL_PORT_PC,
				"Ingrese 'f' para configurar fechas criticas: \r\n");
		encendido_tec();
		break;

	}
	return TRUE;
}


/*Se encarga de ir tres dias para atras de la decha crítica */
/** @fn bool CalculateCriticalDates (tiempo_crit)
 * @brief function in charge of comparing the real date (current) with dates given by the user
 * equal both variables through another function, if both are equal, it returns true, in addition to them, it recognizes the three days prior to that critical date
 * @param [tiempo_crit] parameter type struc
 * @return TRUE if the dates are the same and false if they are not
 *
 */
bool CalculateCriticalDates(tiempo_crit dates_c) {
	bool auxx= false;
	auxx=IsRealTimer(dates_c);
	if(auxx==true) {
	return auxx; }

	tiempo_crit datos;
	uint16_t ms = dates_saved.mess;
	uint16_t da = dates_saved.diaa;
	uint8_t aux = 1;
	int8_t i;
	for (i = 0; i < 3; i++) {
		if (ms % 2 == 0) { /* si mes es par, tengo 30 días */
			if (da < 3) {
				if (da == 1) {
					datos.mess = ms - 1;
					datos.diaa = 30;
					//comparo con fecha real
					auxx=IsRealTimer(datos);
					if(auxx==true) {
						return auxx; }
				} else {
					datos.mess = ms;
					datos.diaa = 1;
					//comparo con fecha real
					auxx=IsRealTimer(datos);
					if(auxx==true) {
						return auxx; }
				}
			} else {
				datos.mess = ms;
				datos.diaa = da - aux;
				auxx=IsRealTimer(datos);
				if(auxx==true) {
					return auxx; }}
		} else { //Si es impart, mes tiene 31 días
			if (da < 3) {
				if (da == 1) {
					datos.mess = ms - 1;
					datos.diaa = 31;
					//comparo con fecha real
					auxx=IsRealTimer(datos);
					if(auxx==true) {
						return auxx; }}
				else {
					datos.mess = ms;
					datos.diaa = 1;
					//comparo con fecha real
					auxx=IsRealTimer(datos);
					if(auxx==true) {
						return auxx; }}
			}
			else {
				datos.mess = ms;
				datos.diaa = da - aux;
				auxx=IsRealTimer(datos);
				if(auxx==true) {
					return auxx; }}
		}
		aux = aux + 1;
	}
    }



/*==================[internal functions declaration]=========================*/

/*Me devuelve true or false si estoy en una fecha crítica  */

/** @fn bool IsRealTimer (tiempo_crit)
 * @brief function defines if we are in an hour o'clock
 * compares the minutes and seconds, if they are equal to zero, then it is an hour at the point
 * @param [tiempo_crit] type structure
 * @return TRUE if the time is on point and FALSE if not
 */
bool IsRealTimer(tiempo_crit datt) {
	if ((datt.mess == hora_config.month) && (datt.diaa == hora_config.mday)) {
		NeoPixelAll(COLOR_G, COLOR_NULL, COLOR_B);
		return true;
	} else {
		return false;
	}

}

/*==================[external functions declaration]=========================*/

/*me devuelve true or false si humedad o temperatura superan el humbral */
/** @fn bool CalculateHTCritical (void)
 * @brief Function calculates if maximum ambient pressure and temperature requirements are met.
 * Used with if, where it asks if it is higher than the maximums
 * @param [in] No parameter
 * @return TRUE if the parameters are met and a FALSE if not.
 */
bool CalculateHTCritical(void) {

	if ((humidity > MAX_HUMID) && (temperature > MAX_TEMP)) {
		NeoPixelAll(COLOR_G, COLOR_R, COLOR_NULL);
		return true;
	} else {
		return false;
	}
}


/*==================[external functions definition]==========================*/
void Detector (void)
{
	int h;
	float max=0; //valor maximo del vector de datos filtrados
	float Umbral; //valor de umbral definido como el valor máximo sobre 3
	float valor_ini=0, valor_final=0;


		for (h=0; h < (ANCHO_VENTANA-1) ; h++)
		{
			if (max < ventana_filtrada [h])
				max = ventana_filtrada [h];
		}
		Umbral= max/4;
	int contador1=0;
	for (i=0; i < ANCHO_VENTANA ; i++)
	{

		if (ventana_filtrada[i] > Umbral && ventana_filtrada[i-1] < Umbral)
		{

					if (contador1==0)
						{
							valor_ini=i;
							contador1++;
						}

					else
					{
						valor_ini= valor_final;
						valor_final=i;
					};
					if (contador1 != 0){

						Frecuencia_Cardiaca = (60*1000/((valor_final-valor_ini)*TIEMPO_MUESTREO)); //Calculo de frecuencia, se que 1Hz equivale a 60 lat/min, por regla de 3 tengo la expresión


						if (Frecuencia_Cardiaca<FC_MIN)

							FC_MIN = Frecuencia_Cardiaca;

						if (Frecuencia_Cardiaca>FC_MAX)

							FC_MAX= Frecuencia_Cardiaca;

						valor_final = valor_ini;
					};

		}
		Umbral=0;
		max =0;
	}; //final del for
 }

/*  FUNCIÓN QUE ME DEVUELVE LOS TRUE OR FALSE DE MI SISTEMA  */
/** @fn bool TakeDate (void)
 * @brief will communicate with the user,
 * will notify if the temperature, humidity and critical date meet
 * with the criteria and if it does it will call the speaker and the stack of leds
 * @param [in] No parameter
 * @return TRUE if there is no error
 */
//analog_input_config Entrada_Analogica ={CH1 , AINPUTS_SINGLE_READ , &ConversionDeDatos_Filtro};
//se me complicó con el tema de las entradas analogicas, como tomo dos datos, lo tengo que ir parando
uint16_t contador_timer=0;
uint16_t contador=0;
bool TakeDate(void) {
	//Me comunico con el usuario para saber la fecha de hoy:
	//mido temperatura y humedad:
	w_LP[0] = 0;w_LP[1] = 0;w_LP[2] = 0;w_LP[3] = 0;w_LP[4] = 0;
	w_HP[0] = 0;w_HP[1] = 0;w_HP[2] = 0;w_HP[3] = 0;w_HP[4] = 0;
		//for (j=0;j < ANCHO_VENTANA; j++){
	  if (j < ANCHO_VENTANA)
		{
		  contador=contador+1;
			AnalogInputRead(CH2, &dato);
				//dato = ecg_data [j];
				output = iir(DL_LP - 1, DEN_LP, NL_LP - 1, NUM_LP, w_LP, dato);    // Filtro pasa bajo
				output2 = iir(DL_HP - 1, DEN_HP, NL_HP - 1, NUM_HP, w_HP, output); // Filtro pasa alto
				//RTPlotDraw (&ecg , output2); // Grafico dato a dato en la señal de ecg

				if (a==1){
					dato_anterior = output2;
					a++;
				}
				else {
						dato_deriv =  (output2 - dato_anterior )/ TIEMPO_MUESTREO;      // Filtro derivativo
						dato_anterior = output2;

						dato_cuadratico = dato_deriv * dato_deriv;                     // Filtro Cuadratico
						ventana_filtrada[a]=dato_cuadratico;   // Guardo datos filtrados en un vector
						a++;j++;
					};
								}
		//}
			if (j == (ANCHO_VENTANA-1)){
				//toma valores cardiacos:
							Detector();
	                        a=0;
							j=0;
									};

    if(contador_timer==CONTADOR) {
	if ((hora_config.min == 0)) {
		//toma los valores de temperatura y humedad
			temperature = TempRead();
			Delay();
			humidity = HumidRead();

		//aviso por uart que ya puedo pregunar si sonar no
			if(encendido) {
				  //bool con comparación datos criticos (humedad y temp.)
		          dates_total_HT = CalculateHTCritical();

	              //pregunto fechas importantes
	              dates_total = CalculateCriticalDates(dates_saved);

	              if ((dates_total_HT == true) && (dates_total == true)) {
	            	  NeoPixelAllRed(BRIGHTNESS);
		              TimerStart(my_timer.timer);
	                                      }
	              if((val_cardiaco->Frec_max >= FREC_MAX) || (val_cardiaco->Frec_min <= FREC_MIN)) {
	            	  NeoPixelAllRed(BRIGHTNESS);
	            	  TimerStart(my_timer.timer);

	                                      }
			             }
			}
    }; //contador
	return TRUE;
};

/* FUNCIÓN QUE HACE SONAR EL PARLANTE  */
/** @fn void doTimer (void)
  * @brief in charge of passing the signal to the speaker so that it sounds.
  * @param [in] No parameter
  * @return does not return
*/
void doTimer(void) {
	static uint32_t i = 0;

	AnalogOutputWrite(song[i]);
	i++;
	if (i == N_SONG) {
		i = 0;
		TimerStop (TIMER_C);
	}
}

/*==================[external functions definition]==========================*/
/* Inicializaciones */
/** @fn bool SisInit (void)
 * @brief Initialization all the functions necessary to carry out the objectives
 * @return nothing return (void)
 */
void SisInit(void) {
	SystemClockInit();
	//SwitchesInit();
	LedsInit();
	//gpio_t gpio_fil={};
	SI7007Init(GPIO_T_FIL0);

	TimerInit(&my_timerTH);
	TimerInit(&my_timer);
	TimerStart(TIMER_B);
	//NeoPixelInit(GPIO_1,8);
	AnalogOutputInit();

	UartInit(&UART_USB);


}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void) {

	SisInit();


	while (1) {

		//stress detector

		}

	return 0;

}

/*==================[end of file]============================================*/

