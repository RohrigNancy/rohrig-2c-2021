/**! @mainpage StressDetector
 * Proyecto libre
 *
 * \section genDesc General Description
 *
 * El programa tendrá como finalidad general detectar ciertos parametros que puedan suponer un aumento del estrés.
 * Para ello se medirá la temperatura y humedad ambiente, se le pedirá al usuario que ingrese fechas que considere críticas
 * y se hara un registro de la frecuencia cardiaca. Una vez detectado estos valores, si el ambiente es propenso a estres,
 * comenzara a sonar una canción "relajane" por un parlante conectado a la EDU_CIAA.
 *
 * \section hardConn Hardware Connection
 *
 * | SI7007: sensor temp./humed.|   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	select		 	     	|  GPIO_T_FIL0	|
 * | 	Vcc: 3.3V	 			| 	3.3V		|
 * | 	PWM2	 				| 	CH1			|
 * | 	PWM1		 			| 	NC			|
 * | 	GND		 			    | 	GND 		|
 *
 * | NeoPixel: Batería de leds  |   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	DIN      	 	     	|   GPIO_1    	|
 * | 	5V  		 			| 	5V			|
 * | 	GND		 			    | 	GND 		|
 *
 * |   Conector para parlante   |   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	DAC     	 	     	|   DAC     	|
 * | 	GND		 			    | 	GND 		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                                             |
 * |:----------:|:------------------------------------------------------------------------|
 * | 01/10/2021 | Document creation		                        					 	  |
 * | 15/10/2021	| Se empieza código de driver SI7007    		     			    	  |
 * | 21/10/2021	| Se completa codigo de comunicación puerto serie                         |
 * | 22/10/2021	| Se detectan errores en driver y UART							      	  |
 * | 31/10/21	| Se agrega song.h y código para que suene el parlante					  |
 * | 04/11/21	| Se modifica el cógigo agregando información para su mejor entendimiento |		           						                     |
 *
 * @author Rohrig Nancy
 *
 */

#ifndef _StressDetector_H
#define _StressDetector_H

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/


/*==================[end of file]============================================*/


#endif /* #ifndef _StressDetector_H */

