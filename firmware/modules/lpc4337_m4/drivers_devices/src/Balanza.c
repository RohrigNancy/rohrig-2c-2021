/**! @mainpage Recuperatorio2C2021_RohrigNancy
 *
 * \section genDesc General Description
 *
 * This application makes the led blink
 *
 * \section hardConn Hardware Connection
 *
 * | Dispositivo1	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Chanel	 	| 	CH1 		|
 * | 	GND  	 	| 	GND 		|
 * | 	+3.3V 	 	| 	+3.3V 		|
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/11/2021 | Document creation		                         |
 * |			|							                     |
 *
 * @author Rohrig Nancy Analia
 *
 */

/*==================[inclusions]=============================================*/
#include <Balanza.h>
#include "gpio.h"
#include "systemclock.h"
#include "bool.h"
#include "analog_io.h"

#ifndef NULL
#define NULL ((void *) 0)
#endif
/*==================[macros and definitions]=================================*/
#define max_voltaje 3.3
#define v_1023 1023
#define peso_max 150
/*==================[internal data declaration]==============================*/
analog_input_config my_adc= {CH1,AINPUTS_SINGLE_READ,NULL};


/*==================[internal data definition]==============================*/

uint16_t Convert_AD_balanza(uint16_t value){
	float voltaje=value*(max_voltaje/v_1023); //te lo devuelve en voltaje

	//utilizando una regla de tres: 3.3V son 150kg(peso máximo soportado)
	// 3.3V _______150kg
	// value_______x=--

	//considero que antes de iniciar la carga, el peso es 0 kg (utilizando "Tara")
	uint16_t peso=((voltaje*peso_max)/max_voltaje);

	return peso;
}

/*==================[external functions definition]==========================*/

bool BalanzaInit(void){

	AnalogInputInit(&my_adc);

	return true;
}


int16_t BalanzaRead(void) {
	uint16_t *Peso_value = 0;
	AnalogInputRead(CH1,&Peso_value);
	int16_t peso=Convert_AD_balanza(Peso_value);

	return peso;
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
