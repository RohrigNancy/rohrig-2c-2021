﻿/* Copyright 2021,
 * Rohrig Nancy Analia
 * Rohriganalia@gmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * El siguiente driver se encargará de la medición del sensor SI7007
 * y su posterior conversión a temperatura en ºC y humedad en %.
 *
 * All rights reserved.
 *
 * \section hardConn Hardware Connection
 *
 * | SI7007: sensor temp./humed.|   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	select		 	     	|  GPIO_T_FIL0	|
 * | 	Vcc: 3.3V	 			| 	3.3V		|
 * | 	PWM2	 				| 	CH1			|
 * | 	PWM1		 			| 	NC			|
 * | 	GND		 			    | 	GND 		|

*/


/*==================[inclusions]=============================================*/
#include "bool.h"
#include "analog_io.h"
#include "gpio.h"

/*==================[macros and definitions]=================================*/
/* NULL pointer */
#ifndef NULL
#define NULL ((void *) 0)
#endif
/*==================[internal data declaration]==============================*/

gpio_t gpio_fil;
analog_input_config my_adc= {CH1,AINPUTS_SINGLE_READ,NULL};


/*==================[internal functions definition]==========================*/
float Convert_AD_temperature(uint16_t value){
	float Tvalue=value*(3.3/1023); //te lo devuelve en voltaje
	float temp=-46.85 + (175.72*(Tvalue/3.3)); //temperatura en celsius

	return (temp-20); //supongo que es un error del medidor
}


float Convert_AD_humidity(uint16_t value){
	float Hvalue=value*(3.3/1023);
	float humid=-6 + (125*(Hvalue/3.3));

	return (humid);
}



/*==================[external functions definition]==========================*/
bool SI7007Init(gpio_t pin)
{
	/* GPIO configurations */
	GPIOInit(pin, GPIO_INPUT);
	AnalogInputInit(&my_adc);

	return true;
}


float TempRead(void)
{
	uint16_t *Tvaluee = 0;
	GPIOOn(gpio_fil);
	AnalogInputReadPolling(CH1,&Tvaluee);
	float temperature=Convert_AD_temperature(Tvaluee);

	return temperature;
}


float HumidRead(void)
{
	uint16_t *Hvalue = 0;
	GPIOOff(gpio_fil);
	AnalogInputReadPolling(CH1,&Hvalue);
	float humidity=Convert_AD_humidity(Hvalue);

	return humidity;
}


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
