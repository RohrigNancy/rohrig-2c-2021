	/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal driver for leds in the EDU-CIAA board.
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 * EF		Eduardo Filomena
 * JMR		Juan Manuel Reta
 * SM		Sebastian Mateos
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20160422 v0.1 initials initial version Leando Medus
 * 20160807 v0.2 modifications and improvements made by Eduardo Filomena
 * 20160808 v0.3 modifications and improvements made by Juan Manuel Reta
 * 20180210 v0.4 modifications and improvements made by Sebastian Mateos
 * 20190820 v1.1 new version made by Sebastian Mateos
 */

/*==================[inclusions]=============================================*/
#include <DisplayITS_E0803.h>
#include "gpio.h"
#include "bool.h"

//#include <stdio.h>
//#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

int16_t valor_guardado;

gpio_t gpio_lcd[4];
gpio_t gpio_sel[3];


/*==================[external data definition]===============================*/


/*==================[external functions definition]==========================*/
 // Función de inicialización

bool ITSE0803Init (gpio_t * pins) {

	gpio_lcd[0]= pins[0];
	gpio_lcd[1]= pins[1];
	gpio_lcd[2]= pins[2];
	gpio_lcd[3]= pins[3];
	gpio_sel[0]= pins[4];
	gpio_sel[1]= pins[5];
	gpio_sel[2]= pins[6];

	GPIOInit(gpio_lcd[0], GPIO_OUTPUT); // inicializamos todos los pines como salidas
	GPIOInit(gpio_lcd[1], GPIO_OUTPUT);
	GPIOInit(gpio_lcd[2], GPIO_OUTPUT);
	GPIOInit(gpio_lcd[3], GPIO_OUTPUT);
	GPIOInit(gpio_sel[0], GPIO_OUTPUT);
	GPIOInit(gpio_sel[1], GPIO_OUTPUT);
	GPIOInit(gpio_sel[2], GPIO_OUTPUT);


	//yo lo había pensado así_>
	//GPIOInit(GPIO_LCD_1, GPIO_OUTPUT); // inicializamos todos los pines como salidas
    //GPIOInit(GPIO_LCD_2, GPIO_OUTPUT);
	//GPIOInit(GPIO_LCD_3, GPIO_OUTPUT);
	//GPIOInit(GPIO_LCD_4, GPIO_OUTPUT);
	//GPIOInit(GPIO_1, GPIO_OUTPUT);
	//GPIOInit(GPIO_3, GPIO_OUTPUT);
	//GPIOInit(GPIO_5, GPIO_OUTPUT);
	//No conveine porque dejo los valores fijos

	return true;
}
/*==================[internal functions definition]==========================*/
void BinaryToBcd(uint32_t data, uint8_t digits, uint8_t * bcd_number)
{
	uint8_t i;
	for(i=0; i<digits; i++)
	{
	bcd_number[i]=data%10; //Lo que hace es dividir el entero que tengamos,
	//quedandonos el número con coma, nos queda el entero y el resto sería lo separado
	data=data/10;
	}

}

/*==================[internal functions definition]==========================*/
////////

void BcdToLed(uint8_t bcd, uint8_t *bcd_separado)
{
	uint8_t i=0;

	for(i=0;i<4;i++) //menor a 4 por que es sólo un digito BCD
	{
		int8_t bcd1;
		bcd1=bcd&(1);

		bcd_separado [i] =bcd1;

		bcd=bcd>>1;

	}

	//return bcd_vector;
}


/*==================[internal data declaration]==============================*/


/*==================[internal functions definition]==========================*/

	//Defina un vector que mapee los bits de la siguiente manera:
	//EDU_CIAA --> Periférico
	    //D1   --> LCD1
	    //D2   --> LCD2
	    //D3   --> LCD3
	    //D4   --> LCD4
	   //SEL_0 --> GPIO1
	   //SEL_1 --> GPIO3
	   //SEL_2 --> GPIO5





/*==================[external functions definition]==========================*/

//función 2 para colocar en la pantalla el número indicado en "valor"
////////////////////////////////////////////////////////////////////////

bool ITSE0803DisplayValue (uint16_t valor)
{
	//Valor guardado para la función ReadValue
	valor_guardado=valor;

	//paso de número binario a número BCD
	uint8_t valor_bcd[3];
	uint8_t lot_digits = 3;
	BinaryToBcd(valor,lot_digits,valor_bcd);

	//Aplico el código mediante mascaras para colocar
	//el número a la pantalla correspondiente -->
    //Además debo activar la pantalla correcta


	uint8_t i=0;
	uint8_t j=0;
	uint8_t bcd_separated[4]= {0,0,0,0};

   for(i=0;i<3;i++) {
	 BcdToLed (valor_bcd[i],&bcd_separated);

	   for(j=0;j<4;j++) //menor a 4 por que es sólo un digito BCD
		  {
			 //printf("Número mostrado %d \r\n", bcd1);
		 GPIOState(gpio_lcd[j],bcd_separated[j]);

		  }

	     GPIOState(gpio_sel[2-i],true);
	     GPIOState(gpio_sel[2-i],false);
	         //printf("Ingresó al if");

       }

  return true;

}

/*==================[external functions definition]==========================*/

uint16_t ITSE0803ReadValue(void) {

	//printf ("El número qué mostraremos es: %d", valor_guardado);

	return valor_guardado;

	
}


/*==================[external functions definition]==========================*/
 // Función de des-inicialización

bool ITSE0803Deinit (gpio_t * pins) {

	GPIODeinit(); // desinicializamos todos los pines como salidas

		return true;

}




//Todavía necesito hacer un main para pasar de la funcion






/*==================[end of file]============================================*/
