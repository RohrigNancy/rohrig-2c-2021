/* Copyright 2021



*/

#ifndef TCRT5000_H
#define TCRT5000_H

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup TCRT5000 Tcrt5000
 ** @{ */

/** @brief Bare Metal header for Tcrt5000 on EDU-CIAA NXP
 **
 ** This is a driver for Tcrt5000 mounted on the board
 **
 **/

/*
 * Initials     Name
 * -----------------------------------------
 *  MVLAP		Molina van Leeuwen Ana Pabla
 */
/*==================[inclusions]=============================================*/

#include <gpio.h>

/*==================[macros]=================================================*/
/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/
/** @brief Definition of constants to reference the EDU-CIAA leds.
 */

/** \brief Definition of constants to control the EDU-CIAA leds.
 **
 **/

/*==================[internal data declaration]==============================*/

/** @fn uint16_t Convert_AD_balanza (value of uint16_t)
  * @brief convert a logic value to a reference voltage
  * @param [uint] a parameter with logical value
  * @return a uint with the voltage
*/
uint16_t Convert_AD_balanza(uint16_t value);


/*==================[external functions declaration]=========================*/

/** @fn bool BalanzaInit (void)
  * @brief EDU-CIAA sensor initialization function
  * Configure the analog channel as input from analogic_io
  * @param [void] does not use parameters
  * @return TRUE if there is no error
*/
bool BalanzaInit(void);

/** @fn int16_t BalanzaRead (void)
  * @brief reads the logic value from the sensor and with the help of another
  * function converts it to weight in kg
  * @param [void] does not use parameters
  * @return an int with the weight
*/
int16_t BalanzaRead(void);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef TCRT5000_H */

