/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 * <a href="https://drive.google.com/file/d/12-Mrw5xsQPZ0wUpekAJImy-Kq2kRgHxW/view?usp=sharing">Operation Example</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	PIN1	 	| 	GPIO3		|
 * | 	PIN2	 	| 	GPIO5		|
 * | 	PIN3	 	| 	GND			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * | 02/01/2020	| A functionality is added	                     |
 * | 			| 	                     						 |
 *
 * @author Albano Peñalva
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

#include "bool.h"
#include "gpio.h"
#include <stdint.h>
/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/
/** @fn bool HcSr04Init(gpio_t echo, gpio_t trigger)
 * @brief Initialization function of EDU-CIAA distance measurer
 * Mapping ports (PinMux function), set direction and initial state of distance measurer ports
 * @param[in] GPIO Echo and GPIO Trigger
 * @return No parameter
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);

/** @fn int16_t HcSr04ReadDistanceCentimeters(void)
 * @brief Reader function of EDU-CIAA for distance in centimeters
 * Mapping ports (PinMux function), set direction and initial state of distance in centimeters reader ports
 * @param[in] No parameter
 * @return int16_t parameter
 */
int16_t HcSr04ReadDistanceCentimeters(void);

/** @fn int16_t HcSr04ReadDistanceInches(void)
 * @brief Reader function of EDU-CIAA for distance in inches
 * Mapping ports (PinMux function), set direction and initial state of distance in inches reader ports
 * @param[in] No parameter
 * @return int16_t parameter
 */
int16_t HcSr04ReadDistanceInches(void);

/** @fn bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
 * @brief Deinitialization function of EDU-CIAA distance measurer
 * Mapping ports (PinMux function), set direction and initial state of leds ports
 * @param[in] GPIO Echo and GPIO Trigger
 * @return TRUE if no error
 */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

