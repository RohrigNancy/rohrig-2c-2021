/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef TCRT5000_H
#define TCRT5000_H

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup TCRT5000 Tcrt5000
 ** @{ */

/** @brief Bare Metal header for Tcrt5000 on EDU-CIAA NXP
 **
 ** This is a driver for Tcrt5000 mounted on the board
 **
 **/

/*
 * Initials     Name
 * -----------------------------------------
 *  MVLAP		Molina van Leeuwen Ana Pabla
 */
/*==================[inclusions]=============================================*/

#include <gpio.h>

/*==================[macros]=================================================*/
/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/
/** @brief Definition of constants to reference the EDU-CIAA leds.
 */

/** \brief Definition of constants to control the EDU-CIAA leds.
 **
 **/

/*==================[external functions declaration]=========================*/

/** @fn bool Tcrt5000Init(gpio_t dout)
 * @brief Initialization function of EDU-CIAA sensor
 * Configura el pin como entrada,para trabajar con el sensor
 * @param[in] dout pin del sensor
 * @return TRUE if no error
 */
bool Tcrt5000Init(gpio_t dout);

/** @fn bool Tcrt5000State(void)
 * @brief Lee el estado de pin del sensor
 * @param[in] No Parameter
 * @return TRUE si esta en alto
 */
bool Tcrt5000State(void);

/** @fn bool Tcrt5000Deinit(gpio_t dout)
 * @brief Desinicializa el pin
 * @param[in] pin del sensor
 * @return TRUE if no error
 */
bool Tcrt5000Deinit(gpio_t dout);
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef TCRT5000_H */

