/* Copyright 2021,
 *
 * Rohrig Nancy
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 *
 * El siguiente driver se encargará de la medición del sensor SI7007
 * y su posterior conversión a temperatura en ºC y humedad en %.
 *
 * All rights reserved.
 *
 * \section hardConn Hardware Connection
 *
 * | SI7007: sensor temp./humed.|   EDU-CIAA	|
 * |:--------------------------:|:--------------|
 * | 	select		 	     	|  GPIO_T_FIL0	|
 * | 	Vcc: 3.3V	 			| 	3.3V		|
 * | 	PWM2	 				| 	CH1			|
 * | 	PWM1		 			| 	NC			|
 * | 	GND		 			    | 	GND 		|

*/

#ifndef SI7007_H
#define SI7007_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Switch
 ** @{ */


/*
 * Initials     Name
 * ---------------------------
 *	NR        Nancy Rohrig
 */


/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>


/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/
/*==================[internal functions definition]==========================*/

/**@brief function to convert from analog to digital the voltage value at temperature.
      * This function receives a value in voltage that passes to temperature
      * @return returns a 16-bit integer with the temperature
 */
float Convert_AD_temperature(int16_t value);

/**@brief function to convert from analog to digital the voltage value at humidity.
      * This function receives a value in voltage that passes to humidity
      * @return returns a 16-bit integer with the humidity
 */
float Convert_AD_humidity(int16_t value);

/*==================[external functions declaration]=========================*/
/**@brief Initialization function of the peripheral Si7007 temperature and humidity meter connected on the EDU-CIAA BOARD
    * This function initializes the four switches present on the EDU-CIAA board
    * @return true if no error
 */
bool SI7007Init(gpio_t *pin);

/** @brief Function to read temperature values
   * The function returns a 16-bit integer, which represents the actual room temperature value
   * @return returns float the temperature value
   */
float TempRead(void);

/** @brief Function to read humidity values
   * The function returns a 16-bit integer, which represents the actual ambient humidity value
   * @return returns float the humidity value
   */
float HumidRead(void);


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef SI7007_H */

